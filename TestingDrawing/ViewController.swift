//
//  ViewController.swift
//  TestingDrawing
//
//  Created by Bilal Mustafa on 2/5/18.
//  Copyright © 2018 Bilal Mustafa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   // @IBOutlet weak var lowerView: UIView!
    
    @IBOutlet weak var newView: UIView!
    //@IBOutlet weak var yellowView: UIView!
    //@IBOutlet weak var upperView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: -0.82, y: 61.12))
        bezierPath.addLine(to: CGPoint(x: 55.18, y: 14.12))
        bezierPath.addLine(to: CGPoint(x: 55.49, y: 13.86))
        bezierPath.addLine(to: CGPoint(x: 55.81, y: 14.1))
        bezierPath.addLine(to: CGPoint(x: 116.81, y: 61.1))
        bezierPath.addLine(to: CGPoint(x: 116.17, y: 61.13))
        bezierPath.addLine(to: CGPoint(x: 161.17, y: 21.13))
        bezierPath.addLine(to: CGPoint(x: 161.49, y: 20.84))
        bezierPath.addLine(to: CGPoint(x: 161.82, y: 21.11))
        bezierPath.addLine(to: CGPoint(x: 210.82, y: 61.11))
        bezierPath.addLine(to: CGPoint(x: 210.13, y: 61.16))
        bezierPath.addLine(to: CGPoint(x: 239.13, y: 29.16))
        bezierPath.addLine(to: CGPoint(x: 240, y: 28.2))
        bezierPath.addLine(to: CGPoint(x: 240, y: 29.5))
        bezierPath.addLine(to: CGPoint(x: 240, y: 119.5))
        bezierPath.addLine(to: CGPoint(x: 240, y: 120))
        bezierPath.addLine(to: CGPoint(x: 239.5, y: 120))
        bezierPath.addLine(to: CGPoint(x: -0.5, y: 120))
        bezierPath.addLine(to: CGPoint(x: -1, y: 120))
        bezierPath.addLine(to: CGPoint(x: -1, y: 119.5))
        bezierPath.addLine(to: CGPoint(x: -1, y: 61.5))
        bezierPath.addLine(to: CGPoint(x: -1, y: 61.27))
        bezierPath.addLine(to: CGPoint(x: -0.82, y: 61.12))
        bezierPath.close()
        bezierPath.move(to: CGPoint(x: -0.18, y: 61.88))
        bezierPath.addLine(to: CGPoint(x: 0, y: 61.5))
        bezierPath.addLine(to: CGPoint(x: 0, y: 119.5))
        bezierPath.addLine(to: CGPoint(x: -0.5, y: 119))
        bezierPath.addLine(to: CGPoint(x: 239.5, y: 119))
        bezierPath.addLine(to: CGPoint(x: 239, y: 119.5))
        bezierPath.addLine(to: CGPoint(x: 239, y: 29.5))
        bezierPath.addLine(to: CGPoint(x: 239.87, y: 29.84))
        bezierPath.addLine(to: CGPoint(x: 210.87, y: 61.84))
        bezierPath.addLine(to: CGPoint(x: 210.55, y: 62.19))
        bezierPath.addLine(to: CGPoint(x: 210.18, y: 61.89))
        bezierPath.addLine(to: CGPoint(x: 161.18, y: 21.89))
        bezierPath.addLine(to: CGPoint(x: 161.83, y: 21.87))
        bezierPath.addLine(to: CGPoint(x: 116.83, y: 61.87))
        bezierPath.addLine(to: CGPoint(x: 116.52, y: 62.15))
        bezierPath.addLine(to: CGPoint(x: 116.19, y: 61.9))
        bezierPath.addLine(to: CGPoint(x: 55.19, y: 14.9))
        bezierPath.addLine(to: CGPoint(x: 55.82, y: 14.88))
        bezierPath.addLine(to: CGPoint(x: -0.18, y: 61.88))
        bezierPath.close()
        UIColor.black.setFill()
        bezierPath.fill()
        
        //bezierPath.fit(into: self.newView.bounds).moveCenter(to: self.newView.bounds.center).fill()
        self.FitPathToRect(path: bezierPath, destRect: self.newView.bounds)
        
        let caShapeLayer = CAShapeLayer()
        caShapeLayer.path = bezierPath.cgPath
        
       // caShapeLayer.frame.size.width = newView.f.size.width
        //caShapeLayer.frame = self.lowerView.frame
        //caShapeLayer.frame = self.yellowView.bounds
        //self.yellowView.layer.addSublayer(caShapeLayer)
        self.newView.layer.mask = caShapeLayer
        
    }
    
    func RectGetCenter(rect: CGRect) -> CGPoint {
        return CGPoint(x: rect.midX, y: rect.midY)
    }
    func RectAroundCenter(center: CGPoint, size: CGSize) -> CGRect {
        let halfWidth: CGFloat = size.width / 2.0
        let halfHeight: CGFloat = size.height / 2.0
        return CGRect(x: center.x - halfWidth, y: center.y - halfHeight, width: size.width, height: size.height)
    }
    func RectCenteredInRect(rect: CGRect, mainRect: CGRect) -> CGRect {
        let dx: CGFloat = mainRect.midX - rect.midX
        let dy: CGFloat = mainRect.midY - mainRect.midY
        return rect.offsetBy(dx: dx, dy: dy)
    }
    func AspectScaleFit(sourceSize: CGSize, destRect: CGRect) -> CGFloat {
        let destSize: CGSize = destRect.size
        let scaleW: CGFloat = destSize.width / sourceSize.width
        let scaleH: CGFloat = destSize.height / sourceSize.height
        return fmin(scaleW, scaleH)
    }
    
    func RectByFittingRect(sourceRect: CGRect, destinationRect: CGRect) -> CGRect {
        let aspect: CGFloat = AspectScaleFit(sourceSize: sourceRect.size, destRect: destinationRect)
        let targetSize = CGSize(width: sourceRect.size.width * aspect, height: sourceRect.size.height * aspect)
        return RectAroundCenter(center: RectGetCenter(rect: destinationRect), size: targetSize)
    }
    
    func ApplyCenteredPathTransform(path: UIBezierPath, transform: CGAffineTransform) {
        let center: CGPoint = RectGetCenter(rect: path.bounds)
        var t: CGAffineTransform = .identity
        t = t.translatedBy(x: center.x, y: center.y)
        t = transform.concatenating(t)
        t = t.translatedBy(x: -center.x, y: -center.y)
        path.apply(t)
    }
    
    
    func OffsetPath(path: UIBezierPath, offset: CGSize) {
        let t = CGAffineTransform(translationX: offset.width, y: offset.height)
        ApplyCenteredPathTransform(path: path, transform: t)
    }
    
    func ScalePath(path: UIBezierPath, sx: CGFloat, sy: CGFloat) {
        let t = CGAffineTransform(scaleX: sx, y: sy)
        ApplyCenteredPathTransform(path: path, transform: t)
    }
    func MovePathCenterToPoint(path: UIBezierPath, destPoint: CGPoint) {
        let bounds: CGRect = path.bounds
        let p1: CGPoint = bounds.origin
        let p2: CGPoint = destPoint
        var vector = CGSize(width: p2.x - p1.x, height: p2.y - p1.y)
        vector.width -= bounds.size.width / 2.0
        vector.height -= bounds.size.height / 2.0
        OffsetPath(path: path, offset: vector)
    }
    
    func FitPathToRect(path: UIBezierPath, destRect: CGRect) {
        let bounds: CGRect = path.bounds
        let fitRect: CGRect = RectByFittingRect(sourceRect: bounds, destinationRect: destRect)
        let scale: CGFloat = AspectScaleFit(sourceSize: bounds.size, destRect: destRect)
        let newCenter: CGPoint = RectGetCenter(rect: fitRect)
        MovePathCenterToPoint(path: path, destPoint: newCenter)
        ScalePath(path: path, sx: scale, sy: scale)
    }
    
    
    
    
    
    
    
    
    
    
   


}

